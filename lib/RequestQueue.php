<?php

namespace Lib;

class RequestQueue extends \ArrayIterator
{
    public function addCall($floor, $direction)
    {
        $this->append(new Call($floor, $direction));
    }

    public function addGo($floor)
    {
        $this->append(new Go($floor));
    }

    public function append($value)
    {
        if ($value instanceof ElevatorAction) {
            return parent::append($value);
        } else {
            throw new \InvalidArgumentException(gettype($value) . ' is not a ElevatorAction');
        }
    }

    public function removeCall($floor)
    {
        foreach($this as $key => $command) {
            if ($command->floor == $floor && $command instanceof Call) {
                unset($this[$key]);
                break;
            }
        }

    }

    public function removeGo($floor)
    {
        foreach($this as $key => $command) {
            if ($command->floor == $floor && $command instanceof Go) {
                unset($this[$key]);
                break;
            }
        }
    }

    public function remove($floor)
    {
        foreach($this as $key => $command) {
            if ($command->floor == $floor) {
                unset($this[$key]);
                break;
            }
        }
    }

    public function issetCommandUp($floor)
    {
        foreach($this as $key => $command) {
            if ($command->floor > $floor) {
                return true;
            }
        }

        return false;
    }

    public function issetCommandDown($floor) {
        foreach($this as $key => $command) {
            if ($command->floor < $floor) {
                return true;
            }
        }
        return false;
    }

    public function issetCommandAtFloor($floor, $direction) {
        foreach($this as $key => $command) {
            if ($command->floor == $floor) {
                /*
                if (isset($command->direction) && null !== $direction) {
                    if ($command->direction === $direction) {
                        return true;
                    }
                } else {
                    return true;
                }
                */

                $this->remove($floor);
                return true;
            }
        }

        return false;
    }
}
