<?php

namespace Lib;

class Elevator
{
    private $floor = 0;
    private $idle = true;
    private $idleFloor = 0;
    private $direction = null;
    private $disabled = false;
    private $door = null;
    private $requestQueue = [];


    public function __construct()
    {
        $this->door = new Door();
        $this->requestQueue = new RequestQueue();
    }

    public function __get($var)
    {
        return $this->$var;
    }

    public function status()
    {
        $queue = [];
        foreach ($this->requestQueue as $req){
            $queue[] = [$req->floor, $req->direction];
        }

        return ['floor' => $this->floor, 'direction' => $this->direction, 'idle' => $this->idle, 'disabled' => $this->disabled, 'door' => $this->door->state,'queue' => $queue];
    }

    public function setFloor($floor)
    {
        $this->floor = $floor;
    }

    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }

    public function isGoingUp()
    {
        return $this->direction === Direction::UP;
    }
    public function isGoingDown()
    {
        return $this->direction === Direction::DOWN;
    }

    public function isAtIdleFloor()
    {
        return $this->floor === $this->idleFloor;
    }

    protected function openDoor()
    {
        $this->door->open();
    }

    protected function closeDoor()
    {
        $this->door->close();
    }

    public function isOpen() {
        return $this->door->isOpen();
    }

    public function isClose()
    {
        return $this->door->isClose();
    }

    protected function assessIdle() {
        $this->idle = (0 === count($this->requestQueue)) ? true : false;
    }

    protected function assessDirection($currentDirection)
    {
        $this->assessIdle();
        $this->direction = ($this->idle) ? null : $currentDirection;
    }

    protected function moveUp()
    {
        $this->floor += 1;
        $this->assessDirection(Direction::UP);
    }

    protected function moveDown()
    {
        $this->floor -= 1;
        $this->assessDirection(Direction::DOWN);
    }

    public function addCall($floor, $direction)
    {
        $this->requestQueue->addCall($floor, $direction);
        $this->assessIdle();
    }

    public function removeCall()
    {
        $this->requestQueue->removeCall($this->floor);
    }

    public function addGo($floor)
    {
        $this->requestQueue->addGo($floor);
    }

    public function removeGo()
    {
        $this->requestQueue->removeGo($this->floor);
        $this->assessIdle();
    }

    public function process()
    {
        if ($this->isOpen()) {
            $this->closeDoor();
            return "Close Door";
        } elseif ($this->requestQueue->issetCommandAtFloor($this->floor, $this->direction)) {
            $this->openDoor();
            return "Open Door";
            //return ($this->isGoingDown() ? "Going Down2" : ($this->isGoingUp()   ? "Going Up2" : "Open Door2"));
        } elseif ($this->requestQueue->issetCommandDown($this->floor) and $this->isGoingDown()) {
            $this->moveDown();
            return "Going Down";
        } elseif ($this->requestQueue->issetCommandUp($this->floor) and $this->isGoingUp()) {
            $this->moveUp();
            return "Going Up";
        } elseif ($this->requestQueue->issetCommandAtFloor($this->floor, null)) {
            $this->openDoor();
            $this->assessDirection(null);
            return "Open Door";
        } elseif ($this->requestQueue->issetCommandDown($this->floor) ) {
            $this->moveDown();
            return "Going Down";
        } elseif ($this->requestQueue->issetCommandUp($this->floor) ) {
            $this->moveUp();
            return "Going Up";
        } else {
            $this->assessIdle();
            $this->assessDirection(null);
            return "Idle";
        }
    }
}
