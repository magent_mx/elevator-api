<?php

namespace Lib;

abstract class ElevatorAction
{
    protected $floor = 1;
    protected $direction = '';

    public function __construct($floor, $direction)
    {
        $this->floor = (int) $floor;
        $this->direction = $direction;
    }

    public function __isset($var)
    {
        return isset($this->$var);
    }

    public function __get($var)
    {
        return $this->$var;
    }
}
