<?php

namespace Lib;

class ElevatorController
{
    private $elevators = [];

    public function __construct($elevators = 2)
    {
        for ($i = 0 ; $i < $elevators; $i++) {
            $this->elevators[] = new Elevator();
        }
    }

    public function __get($var)
    {
        return $this->$var;
    }

    public function status()
    {
        $status = [];
        foreach ($this->elevators as $elevator) {
            $status[] = $elevator->status();
        }

        return $status;
    }

    public function setAt($elevator, $floor)
    {
        $this->elevators[$elevator]->setFloor((int) $floor);
    }

    public function disabled($elevator, $floor)
    {
        $this->elevators[$elevator]->setDisabled((bool) $floor);
    }

    public function call($callFrom, $direction)
    {
        $available = $this->getAvailable();
        if (count($available) == 1) {
            $closest = $available[0];
        } else {
            $closest = $this->getIdleHere($callFrom, $available);

            if (is_null($closest)) {
                $closest = $this->getMovingHere($callFrom, $direction, $available);
            }

            if (is_null($closest)) {
                $closest = $this->getIdleSomewhere($callFrom, $available);
            }

            if (is_null($closest)) {
                $closest = $this->getClosestElevator($callFrom, $direction, $available);
            }
        }

        if (is_null($closest)) {
            throw new \Exception("None available.");
        }

        $this->elevators[$closest]->addCall($callFrom, $direction);
    }

    public function getAvailable()
    {
        $available = [];
        for ($i = 0; $i < count($this->elevators); $i++) {
            if ( ! $this->elevators[$i]->disabled) {
                $available[] = $i;
            }
        }

        return $available;
    }

    public function getIdleHere($floor, $available)
    {
        $closest = null;
        for ($i = 0; $i < count($this->elevators); $i++) {
            if ( ! in_array($i, $available)) {
                continue;
            }

            if ($this->elevators[$i]->idle and $this->elevators[$i]->floor == $floor) {
                $closest = $i;
                break;
            }
        }

        return $closest;
    }

    public function getMovingHere($floor, $direction, $available)
    {
        $closest = null;
        for ($i = 0; $i < count($this->elevators); $i++) {
            if ( ! in_array($i, $available)) {
                continue;
            }

            if ( ! $this->elevators[$i]->idle and $this->elevators[$i]->direction == $direction) {
                if (is_null($closest)) {
                    $closest = $i;
                    continue;
                }

                if ( ! is_null($closest) and abs($this->elevators[$closest]->floor - $floor) > abs($this->elevators[$i]->floor - $floor)) {
                    $closest = $i;
                }
            }
        }

        return $closest;
    }

    public function getIdleSomewhere($floor, $available)
    {
        $closest = null;
        for ($i = 0; $i < count($this->elevators); $i++) {
            if ( ! in_array($i, $available)) {
                continue;
            }

            if ($this->elevators[$i]->idle) {
                if (is_null($closest)) {
                    $closest = $i;
                    continue;
                }

                if ( ! is_null($closest) and abs($this->elevators[$closest]->floor - $floor) > abs($this->elevators[$i]->floor - $floor)) {
                    $closest = $i;
                }
            }
        }

        return $closest;
    }

    public function getAny($available)
    {
        $closest = null;
        for ($i = 0; $i < count($this->elevators); $i++) {
            if ( ! in_array($i, $available)) {
                continue;
            }

            if ( is_null($closest)) {
                $closest = $i;
                break;
            }
        }

        return $closest;
    }

    public function getClosestElevator($floor, $direction, $available)
    {
        $closest = null;
        for ($i = 0; $i < count($this->elevators); $i++) {
            if ( ! in_array($i, $available)) {
                continue;
            }

            if ( is_null($closest)) {
                $closest = $i;
                continue;
            }

            if (abs($this->elevators[$closest]->floor - $floor) > abs($this->elevators[$i]->floor - $floor) and $direction == $this->elevators[$i]->direction) {
                $closest = $i;
            }
        }

        return $closest;
    }

    public function go($elevator, $floor)
    {
        $this->elevators[$elevator]->addGo($floor);
    }

    public function process()
    {
        $result = [];
        foreach($this->elevators as $key => $elevator) {
            $result[$key] = $elevator->process();
        }

        return $result;
    }
}
