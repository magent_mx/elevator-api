<?php

namespace Lib;

class Door
{
    const OPEN = 1;
    const CLOSE = 2;
    private $state = null;

    public function __construct()
    {
        $this->close();
    }

    public function __get($var)
    {
        return $this->$var;
    }

    public function open()
    {
        $this->state = static::OPEN;
        return true;
    }

    public function close()
    {
        $this->state = static::CLOSE;
        return true;
    }

    public function isOpen()
    {
        return static::OPEN === $this->state;
    }

    public function isClose()
    {
        return static::CLOSE === $this->state;
    }
}
