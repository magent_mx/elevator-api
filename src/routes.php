<?php
// Routes
use Slim\Http\Body;
use Lib\ElevatorController;
use Lib\Elevator;

$app->post('/init', function ($request, $response, $args) {
    $body = $request->getParsedBody();

    if ( ! isset($body['elevators']) or ! isset($body['floors'])) {
        return $response->withStatus(400);
    }

    $_SESSION['EC'] = new ElevatorController($body['elevators']);

    $this->logger->info("Initialize");

    return $response->withStatus(200);
});

$app->post('/setAt', function ($request, $response, $args) {
    $body = $request->getParsedBody();

    if ( ! isset($body['elevator']) or ! isset($body['floor'])) {
        return $response->withStatus(400);
    }

    if ( ! isset($_SESSION['EC'])) {
        $_SESSION['EC'] = new ElevatorController();
    }

    $_SESSION['EC']->setAt($body['elevator'], $body['floor']);

    $this->logger->info("send Elevator " . $body['elevator'] . " to " . $body['floor']);

    return $response->withStatus(200);
});

$app->get('/status', function ($request, $response, $args) {

    if ( ! isset($_SESSION['EC'])) {
        $_SESSION['EC'] = new ElevatorController();
    }

    $output = [
        'status' => $_SESSION['EC']->status(),
    ];

    $json = json_encode($output);

    // Ensure that the json encoding passed successfully
    if ($json === false) {
        throw new \RuntimeException(json_last_error_msg(), json_last_error());
    }

    $body = new Body(fopen('php://temp', 'r+'));
    $body->write($json);
    $newResponse = $response->withBody($body);
    $newResponse = $newResponse->withStatus(200)->withHeader('Content-Type', 'application/json;charset=utf-8');

    $this->logger->info("Check elevators");

    return $newResponse;
});

$app->post('/disabled', function ($request, $response, $args) {
    $body = $request->getParsedBody();

    if ( ! isset($body['elevator']) or ! isset($body['disabled'])) {
        return $response->withStatus(400);
    }

    if ( ! isset($_SESSION['EC'])) {
        $_SESSION['EC'] = new ElevatorController();
    }

    $_SESSION['EC']->disabled($body['elevator'], $body['disabled']);

    $this->logger->info("set Elevator " . $body['elevator'] . " to disabled: " . $body['disabled']);

    return $response->withStatus(200);
});

$app->post('/call', function ($request, $response, $args) {
    $body = $request->getParsedBody();

    if ( ! isset($body['callFrom']) or ! isset($body['direction'])) {
        return $response->withStatus(400);
    }

    if ( ! isset($_SESSION['EC'])) {
        $_SESSION['EC'] = new ElevatorController();
    }

    try {
        $_SESSION['EC']->call($body['callFrom'], $body['direction']);
    } catch (\Exception $e) {
        echo $e->getMessage();
    }

    $this->logger->info("Call from floor: " . $body['callFrom'] . " going: " . $body['direction']);

    return $response->withStatus(200);
});

$app->post('/go', function ($request, $response, $args) {
    $body = $request->getParsedBody();

    if ( ! isset($body['elevator']) or ! isset($body['floor'])) {
        return $response->withStatus(400);
    }

    if ( ! isset($_SESSION['EC'])) {
        $_SESSION['EC'] = new ElevatorController();
    }

    try {
        $_SESSION['EC']->go($body['elevator'], $body['floor']);
    } catch (\Exception $e) {
        echo $e->getMessage();
    }

    $this->logger->info("Go in Elevator " . $body['elevator'] . " to floor: " . $body['floor']);

    return $response->withStatus(200);
});

$app->get('/process', function ($request, $response, $args) {
    if ( ! isset($_SESSION['EC'])) {
        $_SESSION['EC'] = new ElevatorController();
    }

    $output = [
        'process_result' => $_SESSION['EC']->process(),
    ];

    $json = json_encode($output);

    // Ensure that the json encoding passed successfully
    if ($json === false) {
        throw new \RuntimeException(json_last_error_msg(), json_last_error());
    }

    $body = new Body(fopen('php://temp', 'r+'));
    $body->write($json);
    $newResponse = $response->withBody($body);
    $newResponse = $newResponse->withStatus(200)->withHeader('Content-Type', 'application/json;charset=utf-8');

    $this->logger->info("Process Step");

    return $newResponse;
});
